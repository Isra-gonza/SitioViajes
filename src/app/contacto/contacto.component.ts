import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ContactService } from '../services/contact.service';
import Swal from 'sweetalert2';

import { User } from '../model/user';
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  User: User;
  contactForm: FormGroup;
  submitted = false;
  loading = false;
  returnUrl: string;
  errorMessage: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private contactService: ContactService,
    private route: ActivatedRoute)
    {

  }

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({
      question: ['', Validators.required],
    });
    
    this.User = JSON.parse(localStorage.getItem('User'));
    this.returnUrl= this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.contactForm.controls; }

  onSubmit() {
    this.submitted = true;
    
    if (this.contactForm.invalid) {
      console.log('Form invalido F');
      return;
    }
    
    this.loading = true;
    this.contactService.submit(this.f.question.value, this.User.id)
    .subscribe(
      data => {

        Swal.fire('Correo envíado correctamente!',`Gracias ${this.User.name}!`, 'success');
        this.router.navigate([this.returnUrl]);
      },
      error => {
          this.errorMessage = error.error.error;
          this.loading = false;
      });
  }
}
