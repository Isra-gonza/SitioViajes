import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

import { AuthenticationService } from '../services/authentication.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  errorTitle: string;
  errorMsg: string;


  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.authenticationService.register(this.f.name.value, this.f.email.value, this.f.password.value)
      .subscribe(
        data => {
          this.authenticationService.login(this.f.email.value, this.f.password.value)
            .subscribe(
              data => {
                Swal.fire(`Bienvenido ${data.User.name}!`,'', 'success');
                  this.errorTitle = null;
                  this.errorMsg = null;
                    this.router.navigate([this.returnUrl]);
              },
              error => {
                console.log(error);
                if (error.status === 401) {
                  this.errorTitle = error.error.title;
                  this.errorMsg = error.error.msg;
                  this.loading = false;
                }
              });
        },
        error => {
          console.log(error);
          if (error.status === 401) {
            this.errorTitle = error.error.title;
            this.errorMsg = error.error.msg;
            this.loading = false;
          }
        });
  }
}
