import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Flight } from '../model/flight';
import { FlightService } from '../services/flight.service';

import { Booking } from '../model/booking';
import { BookingService } from '../services/booking.service';
import { User } from '../model/user';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-vuelo',
  templateUrl: './vuelo.component.html',
  styleUrls: ['./vuelo.component.css']
})
export class VueloComponent implements OnInit {

  reservas: Booking[];
  flight: Flight;
  user: User;
  loginUrl: string;

  constructor(
    private route: ActivatedRoute,
    private FlightService: FlightService,
    private location: Location,
    private router: Router,
    private bookingService: BookingService) { }

  ngOnInit(): void {
    this.getFlight();
    this.loginUrl = '/login';
  }



  getFlight(): void {
    const id = +this.route.snapshot.paramMap.get('id');

    this.FlightService.getFlight(id)
      .subscribe(vuelo => this.flight = vuelo);
  }

  goBack(): void {
    this.location.back();
  }

  booking(flightId: number, quantity: number): void {

    if (!isNaN(quantity)) {

      if (quantity >= 1) {
      var cost = this.flight.price * quantity;
        Swal.fire({
          title: `Seguro que quiere reservar un vuelo para ${quantity} personas?`,
          text: `Le costará ${cost} euros`,
          icon: 'question',
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonText: `Si`,
          denyButtonText: `No`,
        })
        .then((result) => {
         
          if (result.isConfirmed) {

            this.user = JSON.parse(localStorage.getItem('User'));
            
            if (!this.user) {

              Swal.fire('Lo sentimos, antes de reservar tienes que iniciar sesion', '', 'error');
              this.router.navigate([this.loginUrl]);

            }
            else {

                this.bookingService.makeBooking({ created_at: new Date(), updated_at: new Date(), flight_id: flightId, user_id: this.user.id, places: quantity, cost: cost } as Booking)

                  .then((data) => {
                    console.log(data);
                  })
                  .catch((err)=> alert(err.message));

  
              this.bookingService.restPlaces(flightId,quantity)

                .then((data) => {
                  console.log(data);
                  Swal.fire('Reserva guardada', '', 'success')
                })
                .catch((err)=> alert(err.message));


            }

          } else if (result.isDenied) {

            Swal.fire('Proceso de reserva cancelado', '', 'info')
            this.router.navigate([`/flight/${flightId}`])

          }

        })

      }

      else {

        Swal.fire('Por favor introduzca un número mayor a 0', '', 'warning');

      }
      
    }

    else {

      Swal.fire('Introduzca datos válidos', '', 'warning');

    }

  }

}
