import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactoComponent } from './contacto/contacto.component';
import { VueloComponent } from './vuelo/vuelo.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component'
import { UserComponent } from './user/user.component';

const APP_ROUTES: Routes = [
    { path: '', component: HomeComponent },
    { path: 'contact', component: ContactoComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' },
    { path: 'flight/:id', component: VueloComponent },
    { path: 'user', component: UserComponent }
];
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
