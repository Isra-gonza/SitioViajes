export interface Flight {
    id: number;
    started_at: Date;
    finished_at: Date;
    price: number;
    seats: number;
    photo: string;
    departure: string;
    destination: string;
}