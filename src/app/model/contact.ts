export interface Contact {
    subject: String;
    question: String;
}