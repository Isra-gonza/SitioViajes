export interface Booking {
    id: number;
    created_at: Date;
    updated_at: Date;
    places: number;
    cost: number;
    flight_id: number;
    user_id: number;
}