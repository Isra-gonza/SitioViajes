import { Component, OnInit } from '@angular/core';
import { Flight } from '../model/flight';
import { User } from '../model/user';
import { FlightService } from '../services/flight.service';
import { AuthenticationService } from '../services/authentication.service';
import { FilterFlightsService } from '../services/filter-flights.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: User;

  flights: Flight[] = [];
  filteredFlights: Flight|null;


  getFlights():void {
    this.FlightService.getFlights()
      .subscribe(flights => this.flights = flights);
  }
  constructor(private FlightService: FlightService,
    private authenticationService: AuthenticationService,
    private FilterFlightsService: FilterFlightsService) 
    {
    this.authenticationService.currentUser.subscribe(x => this.user = x);
    if (this.FilterFlightsService.currentFilteredFlights) {
      this.FilterFlightsService.currentFilteredFlights.subscribe(y => this.filteredFlights = y);
    }
    }

  ngOnInit() {
    this.getFlights();
  }

}
