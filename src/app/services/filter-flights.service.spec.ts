import { TestBed } from '@angular/core/testing';

import { FilterFlightsService } from './filter-flights.service';

describe('FilterFlightsService', () => {
  let service: FilterFlightsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FilterFlightsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
