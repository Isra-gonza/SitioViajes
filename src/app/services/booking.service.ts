import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Booking } from '../model/booking';
@Injectable({
  providedIn: 'root'
})
export class BookingService {

  private bookingUrl = 'http://api.my/api/bookings/';
  private restPlacesUrl = 'http://api.my/api/restplaces';
  private UserBookings = 'http://api.my/api/bookings/user';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  //Booking POST
  makeBooking(booking: Booking): Promise<Booking>{
    return this.http.post<Booking>(this.bookingUrl,booking).toPromise();
  }

  restPlaces(id:number,quantity: number){
    return this.http.post(`${this.restPlacesUrl}/${id}`,{quantity}).toPromise();
  }

  getUserBookings(id:number) {
    return this.http.get<Booking[]>(`${this.UserBookings}/${id}`).toPromise();
  }

  cancelBooking(id:number):Promise<Booking>{
    return this.http.delete<Booking>(`${this.bookingUrl}${id}`).toPromise();
  }
}
