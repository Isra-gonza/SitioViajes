import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { map } from 'rxjs/operators';

import { Contact } from '../model/contact';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})

export class ContactService {

  User: User;
  private contactUrl = 'http://api.my/api/contact';

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    this.User = this.authenticationService.currentUserValue;
  }

  submit(question: string, user_id: Number) {

    return this.http.post<any>(this.contactUrl, {question, user_id,created_at: new Date(), updated_at: new Date()})
      .pipe(map(data => {
        console.log("Devuelto por server: " + data);
        return data;
      })
      );
  }
}
