import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { Flight } from '../model/flight';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FilterFlightsService {

  private filterUrl = 'http://api.my/api/flights/filter/';

  public currentFilteredFlights: BehaviorSubject<Flight>;
  public filteredFlights: Observable<Flight>;

  constructor(private http: HttpClient) {

    if (this.currentFilteredFlights){
        this.filteredFlights = this.currentFilteredFlights.asObservable();
    }
    else{
      this.currentFilteredFlights = new BehaviorSubject<Flight>(null);
    }
   }

  public get currentFlightValue(): Flight {

    return this.currentFilteredFlights.value;
  }

  search(destination: string) {
    return this.http.get<Flight>(`${this.filterUrl}${destination}`)
    .pipe(map(flight => {
      this.currentFilteredFlights.next(flight);
    }))
  }
}
