import { Injectable } from '@angular/core';
import { Flight } from '../model/flight';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  private flightsUrl = 'http://api.my/api/flights';
  private userFlights = 'http://api.my/api/flights/user';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient) { }

  //GET ALL flights
  getFlights(): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.flightsUrl);
  }

  //GET ONE flight
  getFlight(id:number): Observable<Flight>{
    const url = `${this.flightsUrl}/${id}`;
    return this.http.get<Flight>(url);
  }

  getUserFlights(id:number) {
    return this.http.get<Flight[]>(`${this.userFlights}/${id}`).toPromise();
  }
}
