import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Token } from '../model/token';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private loginUrl = 'http://api.my/api/login';
  private RegisterUrl = 'http://api.my/api/register';
  public currentToken: BehaviorSubject<Token>;
  public Token: Observable<Token>;

  public currentUser: BehaviorSubject<User>;
  public User: Observable<User>;


  constructor(private http: HttpClient) {
    this.currentToken = new BehaviorSubject<Token>(JSON.parse(localStorage.getItem('token')));
    this.Token = this.currentToken.asObservable();

    this.currentUser = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('User')));
    this.User = this.currentUser.asObservable();
  }

  public get currentTokenValue(): Token {

    return this.currentToken.value;
  }

  public get currentUserValue(): User {

    return this.currentUser.value;
  }

  login(email: string, password: string) {
    return this.http.post<any>(this.loginUrl, { email, password })
      .pipe(map(token => {
        // login successful if there's a jwt token in the response
        if (token && token.token) {
          // store token details and jwt token in local storage to keep token logged in between page refreshes
          localStorage.setItem('token', JSON.stringify(token.token));
          localStorage.setItem('User', JSON.stringify(token.User));
          this.currentToken.next(token.token);
          this.currentUser.next(token.User);
        }

        return token;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
    localStorage.removeItem('User');
    this.currentToken.next(null);
    this.currentUser.next(null);
  }

  register(name:string, email:string,password:string){
    return this.http.post<any>(this.RegisterUrl,{name,email,password,created_at: new Date(), updated_at: new Date()})
    .pipe(map(data =>{
      return data;
    }))
  }

}
