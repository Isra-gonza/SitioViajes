import { Component, OnInit } from '@angular/core';

import { User } from '../model/user';
import { Booking } from '../model/booking';

import { FlightService } from '../services/flight.service';
import { BookingService } from '../services/booking.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: User;
  bookings: Booking[] = [];
  userInfo: object[] = [];

  constructor(private FlightService: FlightService, private BookingService: BookingService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('User'));
    this.getUserBookings();
  }

  getUserBookings(): void {
    this.BookingService.getUserBookings(this.user.id)
      .then((data) => {
        if (!data){
          this.bookings = [];
        }
        else {
          this.bookings = data;
        }
      })
      .catch((err) => alert(err.message));
  }

  delete(id: number) {
    Swal.fire({
      title: `Seguro que quiere cancelar la reserva?`,
      icon: 'question',
      showDenyButton: true,
      confirmButtonText: `Si`,
      denyButtonText: `No`,
    })
      .then((result) => {
        if (result.isConfirmed) {
          this.BookingService.cancelBooking(id)
            .then(() => {
              Swal.fire('Reserva cancelada', '', 'success');
            })
            .catch((err) => alert(err.message));
        }
      })

  }
}
