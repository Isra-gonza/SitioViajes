import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { User } from '../model/user';
import { Router } from '@angular/router';

import { FilterFlightsService } from '../services/filter-flights.service';
import { AuthenticationService } from '../services/authentication.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  searchForm: FormGroup;
  User: User;
  filter: string;

  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private filterService: FilterFlightsService,
    private formBuilder: FormBuilder) {
    this.authenticationService.currentUser.subscribe(x => this.User = x);
  }

  ngOnInit() { 
    this.searchForm = this.formBuilder.group({
      destination: ['', Validators.required],
  });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  };

  get f() { return this.searchForm.controls; }

  searchFlights() {

    if (this.searchForm.invalid) {
      Swal.fire('Introduzca datos válidos', '', 'warning');
      return;
  }
  if (!isNaN(this.f.destination.value)){
    Swal.fire('Introduzca datos válidos', '', 'warning');
    return;
  }
    this.filterService.search(this.f.destination.value)
      .subscribe(
        flights => {
          
        },
        error => {
          
        });
  }
}
